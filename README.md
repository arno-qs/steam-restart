steam-restart-script
====================

Overview
--------

The Steam client doesn't have a way to restart on demand.  You used to be able
to create a menu item using skinning, but now that the new version of the
client is out with skin support removed, that's no longer an option.  Unless
(until?) Valve adds a restart option to the client (at least as a menu item,
but ideally as a command-line parameter or "browser protocol" item), this
script attempts to do the best job of gracefully restarting the client
possible.

Installation
------------

Just download the script anywhere and run it; no installation is required.  If
you don't want to clone the whole repository (you only really need the one
file), you can download it here:
[steam-restart-script.sh](https://gitlab.com/arno-qs/steam-restart/raw/master/steam-restart-script.sh?inline=false)

Personally, I use a symlink; you can create one like this (the `...` represents
the directory where you cloned the repository):

```sh
ln --symbolic .../steam-restart/steam-restart-script.sh ${HOME}/.local/bin/steam-restart
```

...but you can just run it directly from the cloned repository if you like.

Configuration
-------------

There's not much to configure, but there are a couple of things.

1. There are a few variables at the top that could be tweaked if your Steam
installation is in a different location, etc.

2. The command to start Steam currently uses `kioclient`; while you technically
_can_ install that outside of KDE, realistically you're probably not going to
want to use it if you're not already using KDE.  So, you'll want to go down to
the bottom, comment that line out, and uncomment one of the other options.  If
this script somehow starts getting attention then I'll look into making it more
agnostic but right now I don't see any reason not to just make it for myself.

Usage
-----

There are no command-line options; simply invoke like this (again, the `...`
represents the directory where you cloned the repository):

```sh
/bin/sh .../steam-restart/steam-restart-script.sh
```

...and the client will be gracefully stopped and started.

Steam client skin
-----------------

This repository also contains the Steam skin I created to add a "Restart Steam"
command to the client, which worked perfectly.  If you're somehow still running
the old version of the client that supports skins, you can copy the
`steam-restart-skin` directory to your skins directory and use it, but as far
as I know it's not useful any more.

There's a couple of files with a ton of stuff in them (because you can't just
specify what you want to change, you have to replace the whole file from
scratch) but all of it really just comes down to this one line in `steam.menu`:

https://gitlab.com/arno-qs/steam-restart/blob/master/steam-restart-skin/resource/menus/steam.menu#L22

So, obviously the actual restart code already exists somewhere (I didn't write
it), the client just needs a mechanism to call it on demand.

To-do
-----

Other than minor tweaks, there isn't really much to do here.  Mostly, the
"to-do" list is for Valve, to implement a proper restart in the client itself.
Ideally, I'd like to see two things added to the client:

1. A "Restart Steam" menu item, like what I implemented in my skin.  If they
only do one thing, it should be this; it's not _my_ preferred solution but
it'll help the most people overall.

2. A way to restart from a script, using either a command-line parameter (e.g.,
`steam --restart`) or the "browser protocol" (e.g., `steam steam://restart`).

#!/bin/sh

## Supporting documentation:
##
## https://developer.valvesoftware.com/wiki/Steam_browser_protocol

steam="/usr/games/steam"
pid_file="${HOME}/.steam/steam.pid"

## Delay between stopping and starting Steam
##
## Five seconds seems to be enough most of the time, but I use 10 for safety.
## This countdown doesn't start until the top-level Steam PID has exited, but
## that's not when it's completely shut down and I don't know of a way to
## check authoritatively, with Steam's zillion child processes.  Set this to
## the highest delay you can tolerate.  :)
delay=10

###############################################################################
###############################################################################

top_level_pid=$(cat ${pid_file})
echo "Read top-level PID from file: ${top_level_pid} (${pid_file})"
echo

echo "Stopping Steam..."
${steam} steam://exit

echo
echo "Pausing between stop and start..."
echo "    Waiting for top-level PID to exit"

while
    ## Steam doesn't clean up its PID file when it exits, so we have to get
    ## a little creative to make sure the process isn't running.
    [ -s "${pid_file}" ]                                         &&
    [ $(pgrep --count --pidfile "${pid_file}" 2>/dev/null) = 0 ] &&
    ps --no-headers -o pid "${top_level_pid}" >/dev/null         ;
do
    sleep 1
done

echo "    Top-level PID exited, adding extra delay (${delay} seconds)"
#sleep ${delay}
countdown ${delay}

echo
echo "Starting Steam..."
#steam
#${steam}
#kioclient exec "file:/usr/share/applications/steam.desktop"
kioclient exec "file:${HOME}/.local/share/applications/steam.desktop"

## EOF
########
